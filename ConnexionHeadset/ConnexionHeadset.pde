//TREES NEEDS TO BE DRAWN ALONE

import processing.serial.*;
import processing.opengl.*;
import toxi.geom.*;
import toxi.processing.*;
import peasy.PeasyCam;
import peasy.org.apache.commons.math.geometry.Rotation;
import peasy.org.apache.commons.math.geometry.RotationOrder;
import peasy.org.apache.commons.math.geometry.CardanEulerSingularityException;
import peasy.org.apache.commons.math.geometry.Vector3D;

import java.util.Locale;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.event.KeyEvent;
import processing.event.MouseEvent;
import processing.opengl.PGraphicsOpenGL;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2ES2;

// Grab orientation via https://github.com/relativty/Relativ
boolean withSTM32 = true;
int lf = 10;    // Linefeed in ASCII
Serial port;                         // The serial port
float[] q = new float[4];
Quaternion quat = new Quaternion(1, 0, 0, 0);

// Moving with Keyboard
int LeftRight = 0;

ToxiclibsSupport gfx;
PeasyCam cam;

boolean walking = false;

int posX, posZ = 0;
PVector forward;
PVector angleForward;

ArrayList <GroundBox> myGround = new ArrayList <GroundBox>();
int tilesGround = 20;
int tilesSize = 500;
int groundY = 400;

//SKY
int tubeRes = 32;
float[] tubeX = new float[tubeRes];
float[] tubeY = new float[tubeRes];
PImage sky;

float cameraZ;

double[] rotationConvert;

PJOGL pgl;
GL2ES2 gl;

Tree tree;

void setup() {

  size(1280, 720, P3D);
  pgl = (PJOGL) beginPGL();  
  gl = pgl.gl.getGL2ES2();
  
  gfx = new ToxiclibsSupport(this);

  // setup lights and antialiasing
  lights();
  smooth();

  if (withSTM32)
  {
    // display serial port list for debugging/clarity
    println(Serial.list());

    // get the first available port (use EITHER this OR the specific port code below)
    String portName = "/dev/ttyACM0";

    // open the serial port
    port = new Serial(this, portName, 250000);
    port.bufferUntil(lf);
  }

  //Ground
  //GROUND
  for (int i =0; i<tilesGround; i++)
  {
    for (int j=0; j<tilesGround; j++)
    {
      myGround.add(new GroundBox(tilesSize, (tilesGround/2*tilesSize)-((i*tilesSize)%(tilesGround*tilesSize)), tilesGround/2*tilesSize-(j*tilesSize)));
    }
  }
  
  //SKY
  sky = loadImage("ciel.png"); //3840*2160
  float angle = 360.0 / tubeRes;
  for (int i = 0; i < tubeRes; i++) {
    tubeX[i] = cos(radians(i * angle));
    tubeY[i] = sin(radians(i * angle));
  }
  cameraZ = ((height/2.0) / tan(PI*60.0/360.0)); 
  
  tree =new Tree(0,0);
}

void draw() {
  // black background
  background(0);
  perspective(PI/3.0,(float) width/height, cameraZ/10.0, cameraZ*1000.0);
  // translate everything to the middle of the viewport
  pushMatrix();
  translate(width / 2, height / 2);
  




  //Grab orientation from MPU6050, via STM32 (see 
  if (withSTM32)
  {
    float[] axis = quat.toAxisAngle();

    double q1 = quat.w;
    double q2 = quat.x;
    double q3 = quat.y;
    double q4 = quat.z;

    Rotation myRotation = new Rotation( q1, q2, q3, q4, true);
    rotationConvert = new double[]{0, 0, 0};

    try
    {
      rotationConvert = myRotation.getAngles(RotationOrder.XYZ);
    }
    catch (CardanEulerSingularityException e)
    {
      println(e);
    }

    rotateX(constrain((float)rotationConvert[1], -0.78, 0.7));
    rotateY((float)rotationConvert[2]);

    // Walking
    forward = PVector.fromAngle((float)rotationConvert[2]);
    forward.rotate(-HALF_PI);
  } else {
    rotateY(radians(LeftRight));
    forward = PVector.fromAngle(radians(LeftRight));
    forward.rotate(-HALF_PI);
  }

  //Debug 
  stroke(255);
  strokeWeight(3);
  //line(3000*forward.x, 400, 3000*forward.y, 0, 400, 0 );

  angleForward = PVector.sub(new PVector(forward.x, groundY, forward.y), new PVector(0, groundY, 0) );
  angleForward.normalize();


  if (walking)
  {
    posX-=15*forward.x;
    posZ-=15*forward.y;
  }
  
  //SKY
  pushMatrix();
  rotateY((float)rotationConvert[2]);
  beginShape(QUAD_STRIP);
  noStroke();
  texture(sky);
  for (int i = 0; i < tubeRes; i++) {
    float x = tubeX[i] * 10000;
    float z = tubeY[i] * 10000;
    float u = sky.width / tubeRes * i;
    vertex(x, -sky.height*10, z, u, 0);
    vertex(x, 0, z, u, sky.height);
  }
  endShape();
  popMatrix();


  translate(posX, 0, posZ);


  // Render Tiles
  //GROUND
  for (GroundBox g : myGround)
  {
    g.returnDist();
    g.display();
  }

  
  // Suppress Tiles
   for (int i =myGround.size()-1; i>0; i--)
   {
   if (myGround.get(i).removable())
   {
   myGround.remove(i);
   println ("Removed "+i);
   }
   }
   
   
  // On cherche le plus loin dans l'axe
  float maxDistTile = 0;
  int farest = 0;

  IntList red = new IntList();

  //Create New Tiles if needed
  for (int j=0; j<myGround.size(); j++)
  {
    /*
    if (maxDistTile<myGround.get(j).getDist())
     {
     maxDistTile = myGround.get(j).getDist();
     farest = j;
     }
     */

    if (myGround.get(j).inFrontOfPlayer())
    {
      red.append(j);
    }
  }
  // On a le plus éloigné
  //println(maxDistTile);
  println(red.size());
  //if (maxDistTile <7000)
  //{
  for (int k = 0; k<red.size(); k++)
  {
    if (myGround.get(red.get(k)).getDist()<15000)
    {
    // On check s'il a des voisins, et si ce n'est pas le cas, on créé ses voisins.
    PVector farestXY = myGround.get(red.get(k)).getXZ();
    //println(farestXY);
    checkNeighbour(int(farestXY.x - tilesSize), int(farestXY.y - tilesSize));
    checkNeighbour(int(farestXY.x), int(farestXY.y - tilesSize));
    checkNeighbour(int(farestXY.x + tilesSize), int(farestXY.y - tilesSize));
    checkNeighbour(int(farestXY.x + tilesSize), int(farestXY.y));
    checkNeighbour(int(farestXY.x + tilesSize), int(farestXY.y + tilesSize));
    checkNeighbour(int(farestXY.x), int(farestXY.y + tilesSize));
    checkNeighbour(int(farestXY.x - tilesSize), int(farestXY.y + tilesSize));
    checkNeighbour(int(farestXY.x - tilesSize), int(farestXY.y));
    }
  }

  /*
  
   */

  println(myGround.size());
  
  
  
  tree.display();
  
  /*
  //PLANE

  // draw front-facing tip in blue
  fill(0, 0, 255, 200);
  pushMatrix();
  translate(0, 0, -120);
  rotateX(PI/2);
  drawCylinder(0, 20, 20, 8);
  popMatrix();

  // draw wings and tail fin in green
  fill(0, 255, 0, 200);
  beginShape(TRIANGLES);
  vertex(-100, 2, 30); 
  vertex(0, 2, -80); 
  vertex(100, 2, 30);  // wing top layer
  vertex(-100, -2, 30); 
  vertex(0, -2, -80); 
  vertex(100, -2, 30);  // wing bottom layer
  vertex(-2, 0, 98); 
  vertex(-2, -30, 98); 
  vertex(-2, 0, 70);  // tail left layer
  vertex( 2, 0, 98); 
  vertex( 2, -30, 98); 
  vertex( 2, 0, 70);  // tail right layer
  endShape();
  beginShape(QUADS);
  vertex(-100, 2, 30); 
  vertex(-100, -2, 30); 
  vertex(  0, -2, -80); 
  vertex(  0, 2, -80);
  vertex( 100, 2, 30); 
  vertex( 100, -2, 30); 
  vertex(  0, -2, -80); 
  vertex(  0, 2, -80);
  vertex(-100, 2, 30); 
  vertex(-100, -2, 30); 
  vertex(100, -2, 30); 
  vertex(100, 2, 30);
  vertex(-2, 0, 98); 
  vertex(2, 0, 98); 
  vertex(2, -30, 98); 
  vertex(-2, -30, 98);
  vertex(-2, 0, 98); 
  vertex(2, 0, 98); 
  vertex(2, 0, 70); 
  vertex(-2, 0, 70);
  vertex(-2, -30, 98); 
  vertex(2, -30, 98); 
  vertex(2, 0, 70); 
  vertex(-2, 0, 70);
  endShape();
  */
  popMatrix();

  if (frameCount % 10 == 0) println("framerate"+ frameRate);
}

void serialEvent(Serial port) {
  String str=port.readString();

  if (str==null) 
    return;

  final float[] vals = float(splitTokens(str, ","));

  // From Relativ : X Y Z W
  quat.set(vals[3], vals[0], vals[1], vals[2]);

  //printArray(vals);
}

void drawCylinder(float topRadius, float bottomRadius, float tall, int sides) {
  float angle = 0;
  float angleIncrement = TWO_PI / sides;
  beginShape(QUAD_STRIP);
  for (int i = 0; i < sides + 1; ++i) {
    vertex(topRadius*cos(angle), 0, topRadius*sin(angle));
    vertex(bottomRadius*cos(angle), tall, bottomRadius*sin(angle));
    angle += angleIncrement;
  }
  endShape();

  // If it is not a cone, draw the circular top cap
  if (topRadius != 0) {
    angle = 0;
    beginShape(TRIANGLE_FAN);

    // Center point
    vertex(0, 0, 0);
    for (int i = 0; i < sides + 1; i++) {
      vertex(topRadius * cos(angle), 0, topRadius * sin(angle));
      angle += angleIncrement;
    }
    endShape();
  }

  // If it is not a cone, draw the circular bottom cap
  if (bottomRadius != 0) {
    angle = 0;
    beginShape(TRIANGLE_FAN);

    // Center point
    vertex(0, tall, 0);
    for (int i = 0; i < sides + 1; i++) {
      vertex(bottomRadius * cos(angle), tall, bottomRadius * sin(angle));
      angle += angleIncrement;
    }
    endShape();
  }
}

void keyPressed()
{
  if (key == CODED) {
    if (keyCode == UP) {
      walking = true;
    } else if (keyCode == LEFT)
    {
      LeftRight-=5;
    } else if (keyCode == RIGHT)
    {
      LeftRight+=5;
    }
  }
}

void keyReleased()
{
  if (key == CODED) {
    if (keyCode == UP) {
      walking = false;
    } else if (keyCode == LEFT)
    {
    } else if (keyCode == RIGHT)
    {
    }
  }
}

void checkNeighbour(int _x, int _y)
{
  boolean somebodyInPlace = false;
  for (int k=0; k<myGround.size(); k++)
  {
    GroundBox myTile = myGround.get(k);
    if (myTile.position(_x, _y))
    {
      somebodyInPlace = true;
    }
  }
  if (!somebodyInPlace)myGround.add(new GroundBox(tilesSize, _x, _y));
}
