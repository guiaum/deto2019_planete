class GroundBox {

  float transparency; // the movement speed and the color transparency
  int myID;
  int size;
  int x, y, z;
  float angle;
  
  boolean removable = false;
  float distance;
  
  boolean position;
  
  boolean inFrontOfPlayer = false;
  
  Tree tree;
  boolean treeCreated = false;

  GroundBox(int _size, int _x, int _z)
  {
    size = _size;

    x= _x;
    y= groundY;
    z= _z;
    println ("Tiles: "+x+" "+y+" "+z);
    /*
    if (random(100)>98)
    {
      tree =new Tree(x,y);
      treeCreated = true;
    }
    */
  }


  void display()
  {
    pushMatrix();
    translate(x, y, z);
    if (treeCreated)tree.display();
    beginShape(QUAD);
    if (degrees(angle) <20)
    {
      fill(175,175,175, transparency);
      inFrontOfPlayer = true;
    }else if (degrees(angle) >=20 && degrees(angle) <=45){
      fill(120,120,120, transparency);
      
    }else{
    fill(100, 100, 100, transparency);
    inFrontOfPlayer = false;
    }
    noStroke();
    vertex(-size/2,0,  -size/2);
    vertex(size/2,0, -size/2);
    vertex(size/2,0, size/2);
    vertex(-size/2,0, size/2);
    
    
    endShape();
    
    //box(size, 2, size);
    popMatrix();
  }

  void returnDist()
  {
    //
    distance = PVector.dist(new PVector(-posX, y, -posZ), new PVector(x, y, z));
    transparency = map(distance, 6000, 3500, 0, 255);

    // To check if it's still visible.
    PVector angleTile = PVector.sub(new PVector(x,y, z), new PVector(-posX, y, -posZ));
    angleTile.normalize();

    angle = PVector.angleBetween(angleForward, angleTile);

    //stroke(255);
    //line (-posX, 0, -posZ, x, y, z);
    
    /*
    if (i==0 && j==0)
    {
      //print(i, j); 
      //print(" ");
      //println(distance);
      //println (transparency);
     // println("forward"+forward);
      //println("angleforward"+angleForward);
      stroke(255);
      line (-posX, y, -posZ, x, y, z);
      noStroke();
      println (degrees(angle));
    }
     */
    if ((degrees(angle)>100) && distance > 5000) removable = true;
    
    //needd to see if its backward or forward
  }
 
  float getDist()
  {
   float returnedDistance = 0;
   if (degrees(angle)<45) returnedDistance = distance;
   return  returnedDistance;
  }
  
  boolean removable()
  {
   return removable; 
  }
  
  boolean position(int _x, int _z)
  {
    if (x==_x && z == _z)
    {
      return true;
    }else
    {
     return false; 
    }
  }
  
  PVector getXZ()
  {
    PVector myXZ = new PVector (x, z);
    return myXZ;
  }
  
  boolean inFrontOfPlayer()
  {
    
    return inFrontOfPlayer;
  }
}
