class Tree 
{

  /**
   * Texture Quad. 
   * 
   * Load an image and draw it onto a quad. The texture() function sets
   * the texture image. The vertex() function maps the image to the geometry.
   */

  PImage img1;
  PImage img2;

  int x;
  int y;


  Tree ( int _x, int _y)
  {
    img1 = loadImage("trees/tree190922_235514_1.png");
    img2 = loadImage("trees/tree190922_235514_2.png");
    x = _x;
    y = _y;
    noStroke();
    noFill();
  }



  void display() {



    /*
    gl.glEnable(GL.GL_BLEND);
gl.glBlendFunc(GL.GL_SRC_ALPHA,GL.GL_ONE_MINUS_SRC_ALPHA);
    */
    pushMatrix();
    
    //gl = pgl.beginGL();
    //gl.glDisable(GL.GL_DEPTH_TEST);
    //gl.glEnable(GL.GL_BLEND);
    //gl.glBlendFunc(GL.GL_SRC_ALPHA,GL.GL_ONE_MINUS_SRC_ALPHA);
    hint(DISABLE_DEPTH_TEST);
    //blendMode(SUBTRACT);
    //translate(0,0,0);
    //translate(0,-groundY,0);
    beginShape();
    texture(img1);
    vertex(-300, -300, 0, 0, 0);
    vertex(300, -300, 0, img1.width, 0);
    vertex(300, 300, 0, img1.width, img1.height);
    vertex(-300, 300, 0, 0, img1.height);
    endShape();

    beginShape();
    texture(img2);
    vertex(0, -300, -300, 0, 0);
    vertex(0, -300, 300, img2.width, 0);
    vertex(0, 300, 300, img2.width, img2.height);
    vertex(0, 300, -300, 0, img2.height);
    endShape();
    //gl.glDisable(GL.GL_BLEND);
    //gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
    hint(ENABLE_DEPTH_TEST);
    popMatrix();
    
    //blendMode(BLEND);

    /*
    
    */
  }
}
